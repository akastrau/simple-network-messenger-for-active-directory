#ifndef HEADERS_H

#define HEADERS_H

/* All main headers */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <locale.h>
#include <assert.h>

#include "uv.h"

/* For DEBUG AND TESTING PURPOSES */
#define error(msg, code) do {                                                         \
  printf("%s: [%s: %s]\n", msg, uv_err_name((code)), uv_strerror((code)));			  \
																					  \
} while(0);


#endif
